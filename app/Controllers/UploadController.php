<?php

include_once 'app/Controllers/RunnableInterface.php';
include_once 'app/Models/User.php';

class UploadController implements RunnableInterface
{
    public function run(): void
    {
        ini_set('post_max_size', '10M');
        ini_set('upload_max_filesize', '10M');

        $this->validateFile();

        $userPath = (new User())->getStoragePath();

        if (!is_dir($userPath)) {
            mkdir($userPath);
        }

        $uploadedFilePath = $userPath . '/'. 'original-uploaded-file.csv';
        move_uploaded_file($_FILES['csv-file']['tmp_name'], $uploadedFilePath);

        $calculatedData = $this->getCalculatedCSVData($uploadedFilePath);
        $this->storeCalculatedCSV($calculatedData, $userPath);
        $this->generaDateFile($userPath);

        (new DisplayController())->run();
    }

    protected function validateFile(): void
    {
        $mimetype = mime_content_type($_FILES['csv-file']['tmp_name']);
        if (!$mimetype === 'text/plain') {
            throw new Exception('Incorrect file format');
        }

        if ($_FILES['csv-file']['size'] > 10485760) { //10mb
            throw new Exception('File must be smaller than 10mb');
        }
    }

    //create file with date as a name to delete old user data later
    protected function generaDateFile(string $userPath): void
    {
        $path = $userPath . '/' . 'created_on_*';
        foreach (glob($path) as $file) {
            unlink($file);
        }
        $dateFileFullPath = $userPath . '/' . 'created_on_' . date('Ymd');
        file_put_contents($dateFileFullPath, '');
    }

    protected function getCalculatedCSVData(string $uploadedFilePath): array
    {
        $resource = fopen($uploadedFilePath, 'r');
        if ($resource === false) {
            exit();
        }

        $calculatedData = [];
        $schema = ['totalPrice' => 0, 'totalNumberOfItems' => 0];

        while(($csvLineData = fgetcsv($resource, 0, ',')) !== false) {
            $this->validateCSVLine($csvLineData);

            list($category, $price, $numberOfItems) = $csvLineData;

            if (!isset($calculatedData[$category])) {
                $calculatedData[$category] = $schema;
            }

            $calculatedData[$category]['totalPrice'] += $price;
            $calculatedData[$category]['totalNumberOfItems'] += $numberOfItems;
        }

        fclose($resource);

        return $calculatedData;
    }

    //todo use DTO for $calculatedData
    protected function storeCalculatedCSV(array $calculatedData, string $userPath): void
    {
        $calculatedFilePath = $userPath . '/' . 'calculated.csv';
        $resource = fopen($calculatedFilePath, 'w');
        foreach ($calculatedData as $category => $calculatedDatum) {
            $fields = [$category, $calculatedDatum['totalPrice'], $calculatedDatum['totalNumberOfItems']];
            fputcsv($resource, $fields);
        }
        fclose($resource);
    }

    protected function validateCSVLine(array $csvLineData): void
    {
        $errorMessage = '';

        if (!count($csvLineData)) {
            $errorMessage .= 'There must be exactly 3 columns. ';
        } else {
            list($category, $price, $numberOfItems) = $csvLineData;

            if (is_string($category) && strlen($category) > 50) {
                $errorMessage .= 'Category string must not be longer then 50 symbols. ';
            }
            if (!is_numeric($price)) {
                $errorMessage .= 'Price must be numeric. ';
            }
            if (!is_numeric($numberOfItems)) {
                $errorMessage .= 'Number of items must be numeric.';
            }
        }

        if ($errorMessage) {
            throw new Exception($errorMessage);
        }
    }
}
