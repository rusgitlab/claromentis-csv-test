<?php

include_once 'app/Controllers/RunnableInterface.php';
include_once 'app/Models/User.php';

class DownloadController implements RunnableInterface
{
    public function run(): void
    {
        $userPath = (new User())->getStoragePath();

        $archiveFilePath = $userPath . '/' .  'download.zip';
        $calculatedFilePath = $userPath . '/'. 'calculated.csv';

        $fileToDownload = $calculatedFilePath;

        if (isset($_GET['type']) && $_GET['type'] === 'zip') {
            $this->prepareArchivedData($archiveFilePath, $calculatedFilePath);
            $fileToDownload = $archiveFilePath;
        }

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($fileToDownload).'"');
        header('Cache-Control: must-revalidate');
        header('Content-Length: ' . filesize($fileToDownload));
        readfile($fileToDownload);
        exit();
    }

    protected function prepareArchivedData($archiveFilePath, $calculatedFilePath): void
    {
        $zip = new ZipArchive;

        if(!file_exists($calculatedFilePath)) {
            echo 'Nothing to download';
            exit();
        };
        if ($zip->open($archiveFilePath, ZipArchive::CREATE) !== TRUE) {
            echo 'Error';
            exit();
        }
        $zip->addFile($calculatedFilePath, 'calculated.csv');
        $zip->close();
    }
}
