<?php

interface RunnableInterface
{
    public function run(): void;
}