<?php

include_once 'app/Controllers/RunnableInterface.php';
include_once 'app/Models/User.php';

class DisplayController implements RunnableInterface
{
    public function run(): void
    {
        echo $this->renderView();
    }

    protected function renderView(): string
    {
        $userPath = (new User())->getStoragePath();
        $calculatedFilePath = $userPath . '/'. 'calculated.csv';
        ob_start();
        include('app/Views/main-view.php');
        return ob_get_clean();
    }
}
