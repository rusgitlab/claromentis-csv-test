<?php


class User
{
    protected string $storagePath = '';

    public function __construct()
    {
        $this->init();
    }

    public function getStoragePath(): string
    {
        return $this->storagePath;
    }

    protected function init(): void
    {
        if (!session_id()) {
            session_start();
        }

        $hashedUserId = md5(session_id());
        $storagePath = 'storage';
        $userPath = $storagePath . '/users/' . $hashedUserId;
        $this->storagePath = $userPath;
    }
}
