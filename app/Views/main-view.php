<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Claromentis CSV calculator</title>
    <meta name="description" content="CSV calculator">
    <meta name="author" content="Dev">
    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
        crossorigin="anonymous"
    >
</head>

<body>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-4">
                <?php
                    if(file_exists($calculatedFilePath)) {
                        $resource = fopen($calculatedFilePath, 'r');
                        if ($resource === false) {
                            echo 'Unable to open file';
                            exit();
                        }

                        echo '
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Category</td>
                                    <td>Price total</td>
                                    <td>Number of items total</td>
                                </tr>
                            </thead>
                            <tbody>
                        ';
                        while(($csvLineData = fgetcsv($resource, 0, ',')) !== false) {
                            list($category, $price, $numberOfItems) = $csvLineData;
                            echo '<tr>';
                            echo sprintf('<td>%s</td><td>%s</td><td>%s</td>', $category, $price, $numberOfItems);
                            echo '</tr>';
                        }
                        echo '
                            </tbody>
                        </table>
                        ';

                        echo '
                            <p>Download this report as CSV - <a href="/download">click here</a></p>
                            <p>Download this report as ZIP - <a href="/download?type=zip">click here</a></p>
                        <br><br>
                        ';
                    }
                ?>

                <form action="/" method="POST" enctype="multipart/form-data">
                    <div class="input-group mb-3">
                        <label class="input-group-text" for="inputGroupFile01">Upload</label>
                        <input accept=".csv" name="csv-file" type="file" class="form-control" id="inputGroupFile01">
                    </div>
                    <input class="btn btn-primary" type="submit" value="Upload">
                    <!-- CSRF field -->
                </form>
            </div>
        </div>
    </div>
</body>

<script
    src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
    crossorigin="anonymous"
>
</script>

</html>
