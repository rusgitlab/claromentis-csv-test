<?php

//this job finds and deletes old users data

const DAYS_IN_WEEK = 7;

$usersPath = 'storage/users';

function rmrf($dir) {
    foreach (glob($dir) as $file) {
        if (is_dir($file)) {
            rmrf("$file/*");
            rmdir($file);
        } else {
            unlink($file);
        }
    }
}

$userDirectories = array_diff(scandir($usersPath), ['.','..']);;

foreach($userDirectories as $userDirectory) {
    $fullUserPath = $usersPath . '/' . $userDirectory;
    $path = $fullUserPath . '/' . 'created_on_*';

    $needToDelete = false;
    foreach (glob($path) as $item) {
        $itemParts = explode('/', $item);
        $dateFileName = array_pop($itemParts);

        $dateFromFileName = str_replace('created_on_', '', $dateFileName);
        $dateCreated = date_create_from_format('Ymd', $dateFromFileName);
        $dateNow = date_create(date('Y-m-d'));
        $diff = date_diff($dateCreated, $dateNow);

        if ($diff->days > DAYS_IN_WEEK) {
            $needToDelete = true;
        };
    }

    if ($needToDelete) {
        rmrf($fullUserPath);
    }
}
