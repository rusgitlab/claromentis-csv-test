<?php

include 'app/Controllers/DisplayController.php';
include 'app/Controllers/UploadController.php';
include 'app/Controllers/DownloadController.php';

class Router
{
    public function run(): void
    {
        $this->getController()->run();
    }

    protected function getController(): RunnableInterface
    {
        if (isset($_FILES['csv-file']) && $_FILES['csv-file']['size'] !== 0) {
            return new UploadController();
        } elseif (strpos($_SERVER["REQUEST_URI"], '/download') !== false) { //rough
            return new DownloadController();
        }
        return new DisplayController();
    }
}

(new Router())->run();
