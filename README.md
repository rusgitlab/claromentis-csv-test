How to use this app:
1. Navigate to / page
2. Choose CSV document that meet the following requirement:
    - The document size is below 10mb
    - Inside, there are only 3 columns with no header: 
        category(string, shorter than 50 symbols),
        price(numeric),
        number of item(numeric),
3. After uploading you will see calculated results on the screen and calculated document
    will be available to download as CSV and as archived ZIP
    
There is cron-job.php file in the root. You can run it once a day to determine user
directories to which nothing has been uploaded for 1 week or more and remove these directories.

Please direct all requests to index.php in you http server configuration.